


#是否引入xlsxwriter库 
#不引入则无需依赖静态库libxlsxwriter.a
#只需要keil5_disp_size_bar.c和keil5_disp_size_bar.h就可以使用gcc直接编译完成
#但是程序的导出xlsx的功能就会无法实现，只能导出csv表格 可使用excel打开自行画图
#如果引入xlsxwriter库 则能导出xlsx表格且画好图

USED_XLSXWRITER_LIB=1
# Keep the output quiet by default.
Q=@
# Directory variables.
INC_DIR = ./inc
# Flags passed to the C++ compiler.
#-Wno-implicit-fallthrough是忽略switch的case没有break的警告
CFLAGS += -g -Wall -Wextra -D USED_XLSXWRITER_LIB=$(USED_XLSXWRITER_LIB) -Wno-implicit-fallthrough
# Source files to compile.
SRCS = ./src/*.c 
APP = keil5_disp_size_bar.exe 
LIBXLSXWRITER = ./lib/libxlsxwriter.a


ifeq ($(USED_XLSXWRITER_LIB), 1)
    LIBS += $(LIBXLSXWRITER)
endif

LIBS += -lz

#-lz是链接 zlib 库默认mingw自带了，且-lz必须是在libxlsxwriter.a后面连接不然就会有东西找不到定义

all :  $(APP)

$(APP): $(SRCS) $(LIBXLSXWRITER)
	$(CC) -I$(INC_DIR) $(CFLAGS) $< -o $@ $(LIBS)

clean :
	$(Q)del -f $(APP)

